$(document).ready(function(){
    
$('.pizza-type label').hover(

        function () {
            $(this).find(".description").show();
        },
        function () {
            $(this).find(".description").hide();
        }
    )

    $('.nb-parts input').on('keyup', function () {

        const pizza = $('<span class="pizza-pict"></span>');
        nbParts = +$(this).val();

        $(".pizza-pict").remove();

        for(let i = 0; i<nbParts/6 ; i++){

            $(this).after(pizza.clone().addClass("pizza-6"));
        }

        if(nbParts%6 !== 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-' + nbParts%6);
        price();
    })

    $('.next-step').click(function () {
        $('.infos-client').slideDown();
        $(this).hide();
    })

    const input = $('<br><input type="text"/>');

    $('.add').click(function () {
        $(this).before(input.clone());
    })

    $('.done').click(function () {

        const name = $('#name').val();
        const thanks = $('<span>Merci ' + name + ' ! Votre commande sera livrée dans 15 minutes</span>');
        $('.main').slideUp();
        $('.headline').append(thanks);
    })
});